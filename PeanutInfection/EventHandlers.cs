﻿using EXILED;
using UnityEngine;
using MEC;
using System.Collections.Generic;
using System;
using GameCore;

namespace PeanutInfection
{
    public class EventHandlers
    {
        private Main _main;

        public EventHandlers(Main main)
        {
            _main = main;
        }
        public void Event_PlayerDeathEvent(ref PlayerDeathEvent ev)
        {
            if (!_main.IsEnabled) return;

            if (ev.Killer.characterClassManager.Scp173.iAm173)
            {
                Vector3 deathPos = ev.Killer.plyMovementSync.RealModelPosition;
                ev.Player.characterClassManager.SetClassID(RoleType.Scp173);
                ev.Player.playerStats.SetHPAmount(Config.InfectedHP);
                Timing.RunCoroutine(DoTeleport(ev.Player, deathPos));
            }
        }

        public void Events_TeamRespawnEvent(ref TeamRespawnEvent ev)
        {   
            if (!_main.IsEnabled) return;
            if (Config.EnableRespawn) return;
            ev.ToRespawn = new List<ReferenceHub>();
        }

        public void Event_PlayerSpawnEvent(PlayerSpawnEvent ev)
        {
            if (!_main.IsEnabled) return;
            if (ev.Role.IsAnySCP() && ev.Role != RoleType.Scp173)
            {
                Timing.RunCoroutine(DoChangeScp(ev.Player));
            }
                
        }

        private IEnumerator<float> DoChangeScp(ReferenceHub rh)
        {
            yield return Timing.WaitForSeconds(1f);

            CharacterClassManager cm = PlayerManager.localPlayer.GetComponent<CharacterClassManager>();
            rh.characterClassManager.SetClassID(RoleType.Scp173);
            rh.playerStats.SetHPAmount(cm.Classes.SafeGet(rh.characterClassManager.CurClass).maxHP);
            rh.plyMovementSync.TargetForcePosition(rh.scp079PlayerScript.connectionToClient, CharacterClassManager.SpawnpointManager.GetRandomPosition(RoleType.Scp173).transform.position);

            yield break;
        }
        private IEnumerator<float> DoTeleport(ReferenceHub rh, Vector3 pos)
        {
            yield return Timing.WaitForSeconds(1f);
            rh.plyMovementSync.TargetForcePosition(rh.scp079PlayerScript.connectionToClient, pos);
            yield break;
        }
    }
}
